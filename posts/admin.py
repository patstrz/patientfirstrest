from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

# Register your models here.
from .models import Post,TaggedItem,SubPost

# class CommentAdmin(admin.ModelAdmin):
#     list_display = ['__unicode__', 'text']
#     class Meta:
#         model = Comment
#
# admin.site.register(Comment, CommentAdmin)

class TaggedItemInline(GenericTabularInline):
    model = TaggedItem

# class SubPostInline(GenericTabularInline):
#     model = SubPost

class PostAdmin(admin.ModelAdmin):
    inlines = [TaggedItemInline]
    list_display = ['__unicode__', 'slug', 'title']
    # fields = ['title']
    class Meta:
        model = Post


admin.site.register(Post,PostAdmin)
admin.site.register(TaggedItem)
admin.site.register(SubPost)
