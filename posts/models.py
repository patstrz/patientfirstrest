from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save
from accounts.models import MyUser
from django.utils.text import slugify
from patientfirst import settings


class Post(models.Model):
    subposts = models.ManyToManyField('SubPost')
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=500)
    slug = models.SlugField(blank=True,null = True,default = "abc") # slug created post_save
    tags = GenericRelation("TaggedItem", blank=True, null=True)
    #safe_guard the slug field such that each is unique so that don't get ajax errors.
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="likes")
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    active = models.BooleanField(default=True)
    image = models.ImageField(upload_to='images/', null=True, blank=True)
    content = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.title

    def total_likes(self):
        return int(self.likes.count())

    # Get absolute url function here
    def get_image_url(self):
        return "%s%s" % (settings.MEDIA_URL, self.image)

    def get_absolute_url(self):
        return reverse("post_detail", kwargs={"post_slug": self.slug})



def post_save_receiver_for_post(sender, instance, created, *args, **kwargs):
    if created:
        slug = slugify("%s %s" % (instance.title, instance.id ))
        instance.slug = slug
        instance.save()

    # print instance.slug

TAG_CHOICES= (
("python","python"),
("django","django"),
("new","new"),
("new2","new2"),
("wearable", "wearable"),
("prosthesis", "prosthesis"),
("pediatric", "pediatric")
)



class TaggedItem(models.Model):
    tag = models.SlugField(choices=TAG_CHOICES)
    content_type = models.ForeignKey(ContentType)

    #put the following code into post object:
    # content_type = ContentType.objects.get_for_model()
    object_id = models.PositiveIntegerField()
    #  do unique-together for 'tag,content_type and object_id'
    content_object = GenericForeignKey()

    def __unicode__(self):
        return self.tag
    # ToDo do uniques ( unique together?)
    # may want to makemore robust in the future by checking if exists like in srvup videos.models

post_save.connect(post_save_receiver_for_post, sender=Post)


class SubPost(models.Model):

    title = models.CharField(max_length=30)
    description = models.CharField(max_length=500)
    slug = models.SlugField(blank=True,null = True,default = "abc") # slug created post_save
    # tags = GenericRelation("TaggedItem", blank=True, null=True)
    #Todo safe_guard the slug field such that each is unique so that don't get ajax errors.
    # likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="likes")
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    active = models.BooleanField(default=True)
    image = models.ImageField(upload_to='images/', null=True, blank=True)

    def __unicode__(self):
        return self.title

    def get_image_url(self):
        return "%s%s" % (settings.MEDIA_URL, self.image)