
from rest_framework import routers, serializers, viewsets, permissions

from .models import Post, SubPost, TaggedItem
from accounts.models import MyUser
from rest_framework.reverse import reverse
from comments.serializers import CommentSerializer




class UserObjectSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = MyUser
        fields = [
            'username'
        ]


class SubPostsSerializer(serializers.HyperlinkedModelSerializer):
    #todo add content, comments ...
    class Meta:
        model = SubPost
        fields =[
            'title',
            'description',
            'image',
        ]

class PostUrlField(serializers.HyperlinkedIdentityField):
    # the kwargs is used to provide the lookup argument
    def get_url(self, obj, view_name, request, format):
        kwargs = {

            'post_slug': obj.slug

        }
        return reverse(view_name, kwargs=kwargs, request=request, format=format)


class PostsSerializer(serializers.HyperlinkedModelSerializer):
    # tags = serializers.RelatedField(many=True)
    url = PostUrlField(view_name='post_detail_api', required=False)
    subposts = SubPostsSerializer(many=True)
    likes = UserObjectSerializer(many=True)
    comment_set = CommentSerializer(many=True,read_only=True)
    class Meta:
        model = Post
        fields =[
            'url',
            'id',
            'title',
            'slug',
            'description',
            'image',
            'subposts',
            'likes',
            'content',
            # 'tags',
            'comment_set'
        ]

# class PostsDetailSerializer(serializers.HyperlinkedModelSerializer):
#     # tags = TagSerializer(serializers.RelatedField(source='tag'))
#     # queryset =
#     # tags = serializers.RelatedField(many=True)
#     subposts = SubPostsSerializer(many=True)
#     likes = UserObjectSerializer(many=True)
#
#     class Meta:
#         model = Post
#         fields =[
#             'id',
#             'title',
#             'slug',
#             'description',
#             'image',
#             'content',
#             'subposts',
#             'likes'
#             # 'tags',
#         ]

class PostsUpdateDeleteSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Post
        fields =[
            'id',
            'title',
            'slug',
            'description',
            'image',
            'content'
        ]


class PostsLikesSerializer(serializers.HyperlinkedModelSerializer):
    likes = UserObjectSerializer(many=True)

    class Meta:
        model = Post
        fields = [
            'id'
            'likes'
        ]
    # def validate_likes[]

class PostsNewSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Post
        fields =[
            'title',
            'description',
            'image',
            'content'

        ]


