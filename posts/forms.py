from django import forms

class NewPost(forms.Form):
    title = forms.CharField(label='Title', max_length=100)
    description = forms.CharField(widget=forms.Textarea)

class NewSubPost(forms.Form):
    title = forms.CharField(label='Title', max_length=100)
    description = forms.CharField(widget=forms.Textarea)
    image = forms.ImageField(widget=forms.ImageField)
