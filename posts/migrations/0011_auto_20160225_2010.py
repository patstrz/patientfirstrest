# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0010_auto_20160225_2009'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='parent_post',
            new_name='subposts',
        ),
    ]
