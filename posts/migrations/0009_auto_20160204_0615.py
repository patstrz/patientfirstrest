# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0008_auto_20160203_2333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taggeditem',
            name='tag',
            field=models.SlugField(choices=[(b'python', b'python'), (b'django', b'django'), (b'new', b'new'), (b'new2', b'new2'), (b'wearable', b'wearable'), (b'prosthesis', b'prosthesis'), (b'pediatric', b'pediatric')]),
        ),
    ]
