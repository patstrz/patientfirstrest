# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0009_auto_20160204_0615'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=30)),
                ('description', models.CharField(max_length=500)),
                ('slug', models.SlugField(default=b'abc', null=True, blank=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('active', models.BooleanField(default=True)),
                ('image', models.ImageField(null=True, upload_to=b'images/', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='post',
            name='parent_post',
            field=models.ManyToManyField(to='posts.SubPost'),
        ),
    ]
