
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, Http404, HttpResponse, HttpResponseRedirect, get_object_or_404
from .forms import NewPost
from comments.forms import CommentForm
from .models import Post
from django.db import models
from .serializers import PostsSerializer, PostsNewSerializer, PostsUpdateDeleteSerializer
from rest_framework import generics, permissions, mixins
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from accounts.models import MyUser
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
# ------------API

# def like(request, post_slug, username):


# idealy this url will handle both get and post where the GET rewquest will give the likes count and POST will
# like or unlike a post.

@api_view(['GET'])
# @permission_classes((IsAuthenticated, ))
def like(request,post_slug):
    username = request.user
    print username
    like_status = None

    if request.method == 'GET':
        user = get_object_or_404(MyUser, username=username)
        post = get_object_or_404(Post, slug=post_slug)

        if post.likes.filter(username=username).exists():
            post.likes.remove(user)
            like_status = False
        else:
            post.likes.add(user)
            like_status = True
        likes_count = post.total_likes()

        return Response({"likes_count": likes_count, "like_status":like_status})


class PostsCreateAPIView(generics.CreateAPIView):
    serializer_class = PostsNewSerializer
    permission_classes = [permissions.AllowAny, ]
    #todo change permission to IsAuthenticated


class PostListAPIView(generics.ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication]
    queryset = Post.objects.all()
    serializer_class = PostsSerializer
    permission_classes = [permissions.IsAuthenticated, ]



class PostDetailAPIView(mixins.DestroyModelMixin, generics.RetrieveAPIView):
    # didn't need to set authentication class because of default settings
    queryset = Post.objects.all()
    serializer_class = PostsSerializer
    permission_classes = [permissions.IsAuthenticated]
    # lookup_field = 'slug'


    def get_object(self, *args, **kwargs):
        post_slug = self.kwargs['post_slug']
        obj = get_object_or_404(Post, slug=post_slug)
        return obj

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)



class UpdateDetailAPIView(mixins.DestroyModelMixin, mixins.UpdateModelMixin, generics.RetrieveAPIView):
    # didn't need to set authentication class because of default settings
    queryset = Post.objects.all()
    serializer_class = PostsUpdateDeleteSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'slug'

    def get_object(self, *args, **kwargs):
        post_slug = self.kwargs['post_slug']
        obj = get_object_or_404(Post, slug=post_slug)
        return obj

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)












# -------------API
def new_post(request):

    form = NewPost(request.POST or None)

    if form.is_valid():

        title = form.cleaned_data['title']
        description = form.cleaned_data['description']
        new_instance = Post(title=title, description = description)
        # new_instance.title = title
        # new_instance.description = description
        new_instance.save()

        return redirect(reverse('home'))

    context = {'form': form, }



    return render(request, 'posts/new_post.html', context)


def category_detail(request,post_slug ):
    comment_form = CommentForm()

    #So will not have non-existent queries when db is empty
    try:
        post_object = Post.objects.get(slug = post_slug)
        subposts = post_object.subposts.all()
        comment_objects = post_object.comment_set.all()
    except:
        post_object = None
        subposts = None
        comment_objects = None

    context = {"post": post_object,
               'comment_form':comment_form,
               "comments":comment_objects,
               "subposts":subposts}
    print 'subposts:',subposts

    if subposts:

        for post in subposts:
            image = post.get_image_url()
            print post.title
            print 'image url:',image

    return render(request, 'posts/category_detail.html', context)
