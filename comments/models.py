from django.db import models
from accounts.models import MyUser
from posts.models import Post
from django.core.urlresolvers import reverse


class Comment(models.Model):
    user = models.ForeignKey(MyUser)
    parent_comment = models.ForeignKey("self", null=True, blank=True)
    comment_text = models.TextField()
    # path = models.CharField(max_length=350)
    post = models.ForeignKey(Post, null=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.comment_text

    def get_absolute_url(self):
        return reverse('comment_thread', kwargs={"id": self.id})

    def is_child(self):
        if self.parent_comment is not None:
            return True
        else:
            return False