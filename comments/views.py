from django.contrib.contenttypes.models import ContentType
from django.http import Http404
from django.shortcuts import render, HttpResponseRedirect
from .forms import CommentForm
from .models import Comment
from posts.models import Post

from notification.signals import notify

from .serializers import CommentSerializer, CommentCreateSerializer, CommentUpdateSerializer
from rest_framework import generics, permissions, mixins
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from permissions import IsOwnerOrReadOnly



# API CODE

class CommentCreateAPIView(generics.CreateAPIView):
    serializer_class = CommentCreateSerializer

class CommentDetailAPIView(mixins.DestroyModelMixin, mixins.UpdateModelMixin, generics.RetrieveAPIView):
    permission_classes = (IsOwnerOrReadOnly, )
    queryset = Comment.objects.all()
    serializer_class = CommentUpdateSerializer
    lookup_field = 'id'  # uses id keyword argument that is passed in from the url

    def get_queryset(self):
        queryset = Comment.objects.filter(pk__gte=0)
        # queryset = Comment.objects.all()

        # to give you an all objects that includes children. all objects with a pk
        return queryset

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class CommentListAPIView(generics.ListAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication]
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated, ]



# API CODE END

# Create your views here.
def new_comment(request):
    if request.method == "POST" and request.user.is_authenticated():
        form = CommentForm(request.POST or None)
        if form.is_valid():
            comment = form.cleaned_data['comment']
            current_path = request.POST.get("current_path")
            post_id = request.POST.get("post_id")
            parent_comment_id = request.POST.get("parent_comment_id")
            user = request.user

            parent_comment_obj = None

            if parent_comment_id is not None:
                try:
                    parent_comment_obj = Comment.objects.get(id=parent_comment_id)
                except:
                    parent_comment_obj = None

            try:
                post_instance = Post.objects.get(id=post_id)

            except:
                post_instance = None


            if post_instance and parent_comment_obj:

                # print 'we have post and parent'

                new_comment = Comment(comment_text = comment,
                                   user = user,
                                   post = post_instance,
                                   parent_comment = parent_comment_obj,
                                      )
                new_comment.save()
                #being connected to notification.models.new_notification
                notify.send(request.user,
                            recipient=parent_comment_obj.user,
                            verb='commented',
                            action=new_comment,
                            target=parent_comment_obj)

            else:

                new_comment = Comment(comment_text = comment,
                                   user = user,
                                   post = post_instance,
                                      )
                new_comment.save()
                notify.send(request.user,
                            recipient=request.user,
                            verb='commented',
                            action =new_comment,
                            target = new_comment.post
                            )
            return HttpResponseRedirect(current_path)

        # else:
        #     raise Http404

    else:
        raise Http404

def comment_thread(request,id):
    print id

    context ={'id':id}
    return render(request, "comments/comment_thread.html", context)


        # print current_path, post_id



# def sub_comment(request):
#     form = CommentForm(request.POST or None):
#     user = request.user
#     # user = user.id
#     comment = None
#     current_path = None
#
#     if form.is_valid():
#         comment = form.cleaned_data['comment']
#         current_path = request.POST.get("current_path")
#         post_id = request.POST.get("post_id")
#     print 'comment:',comment, 'user":', user

