
from rest_framework import serializers, permissions
from rest_framework.reverse import reverse
from .models import Comment




class CommentUpdateSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.username', read_only=True)

    class Meta:
        model = Comment
        fields = [
            'id',
            'user',
            'comment_text',
        ]

class CommentCreateSerializer(serializers.ModelSerializer):
    # video = VideoUrlField

    class Meta:
        model = Comment
        fields = [
            'comment_text',
            'user',
            'post',
            'parent_comment',
        ]


class ChildCommentSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.CharField(source='user.username', read_only=True)

    class Meta:
        model = Comment
        fields = [
            'id',
            "user",
            'comment_text',
        ]

class CommentPostUrlField(serializers.HyperlinkedIdentityField):
    # the kwargs is used to provide the lookup argument
    def get_url(self, obj, view_name, request, format):
        post = None
        if obj.is_child():
            try:
                post = obj.parent.post
            except:
                post = None
        else:
            try:
                post = obj.post
            except:
                post = None

        if post:

            kwargs = {
                'post_slug': post.slug,

            }
            return reverse(view_name, kwargs=kwargs, request=request, format=format)
        else:
            return None

class CommentUrlField(serializers.HyperlinkedIdentityField):
    # the kwargs is used to provide the lookup argument
    def get_url(self, obj, view_name, request, format):
        kwargs = {

            'id': obj.id

        }
        return reverse(view_name, kwargs=kwargs, request=request, format=format)

class CommentSerializer(serializers.HyperlinkedModelSerializer):

    # user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    post = CommentPostUrlField(view_name='post_detail_api')
    user = serializers.CharField(source='user.username', read_only=True)
    children = serializers.SerializerMethodField(read_only=True)
    comment_url = CommentUrlField(view_name= 'comment_detail_api')

    def get_children(self, instance):
        # queryset = instance.get_children()
        user = serializers.CharField(source='user.username', read_only=True)
        queryset = Comment.objects.filter(parent_comment__pk=instance.pk)
        serializer = ChildCommentSerializer(queryset, context={"request": instance}, many=True)
        return serializer.data

    class Meta:
        model = Comment
        fields = [
            # "url",
            'id',
            'comment_url',
            'post',
            "user",
            "comment_text",
            "children",
        ]
