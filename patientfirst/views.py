from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, HttpResponseRedirect
from django.utils.safestring import mark_safe
from .forms import LoginForm
from settings import AUTH_USER_MODEL
from settings import PROJECT_ROOT,MEDIA_ROOT

from posts.models import TaggedItem



@login_required()
def home(request):

    name = request.user
    from posts.models import Post
    posts = Post.objects.all().order_by('-timestamp')
    # post = Post.objects.get(slug = 'jan-3-42')
    # content_type = ContentType.objects.get_for_model(post)
    # tags = TaggedItem.objects.filter(content_type=content_type, object_id=42)
    # print tags
    query_set = []
    for post in posts:
        if post.likes.filter(id=name.id).exists():
            query_set.append({'post':post,'like':'liked'})

        else:
             query_set.append({'post':post,'like':'not_liked'})

    context = {"the_name": name, "posts": posts, "query_set": query_set}
    return render(request, "home.html", context)

def auth_login(request):
    #
    form = LoginForm(request.POST or None)

    next_url = request.GET.get('next')

    if form.is_valid():

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request,user)

            if next_url is not None:

                return HttpResponseRedirect(next_url)

            else:
                return HttpResponseRedirect('/')

    context = {"forms": form}
    return render(request, "login.html", context)

def auth_logout(request):
    logout(request)
    return HttpResponseRedirect('/')