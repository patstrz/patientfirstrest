import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "patientfirst.settings")

#must set "DJANGO_SETTINGS_MODULE" before importing whitenoise to avoid 503 error

# from whitenoise.django import DjangoWhiteNoise

application = get_wsgi_application()
# application = DjangoWhiteNoise(application)

