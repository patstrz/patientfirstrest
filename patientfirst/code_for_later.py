__author__ = 'patrickstrzelec'

#in models.py ###################

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

class TaggedItem(models.Model):
    tag = models.SlugField(choices=TAG_CHOICES)
    content_type = models.ForeignKey(ContentType)
    #put the following code into post object:
    # content_type = ContentType.objects.get_for_model()
    object_id = models.PositiveIntegerField()
    #  do unique-together for 'tag,content_type and object_id'
    content_object = GenericForeignKey()

    def __unicode__(self):
        return self.tag
    # Do uniques



#in views. py ###################

from django.contrib.contenttypes.models import ContentType
from posts.models import TaggedItem

post = Post.objects.get(slug = 'jan-3-42')
content_type = ContentType.objects.get_for_model(post)
tags = TaggedItem.objects.filter(content_type=content_type, object_id=42)
print tags

#in admin.py for any of the models ################

from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

# Register your models here.
from .models import Post,TaggedItem

# class CommentAdmin(admin.ModelAdmin):
#     list_display = ['__unicode__', 'text']
#     class Meta:
#         model = Comment
#
# admin.site.register(Comment, CommentAdmin)

class TaggedItemInline(GenericTabularInline):
    model = TaggedItem