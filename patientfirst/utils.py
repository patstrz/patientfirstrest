from storages.backends.s3boto import S3BotoStorage
from rest_framework_jwt.utils import jwt_response_payload_handler

StaticRootS3BotoStorage = lambda: S3BotoStorage(location='static')
MediaRootS3BotoStorage  = lambda: S3BotoStorage(location='media')

# def jwt_response_payload_handler(token, user=None, request=None):
#     return {
#         'token': token,
#         'user': UserSerializer(user).data
#     }

def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': str(user.username)
    }

#todo switch to email later ^