
# from django.conf import settings
from django.conf.urls import patterns,include, url
from django.conf.urls.static import static

from django.conf import settings
from django.contrib import admin
from accounts.views import AccountCreateAPIView
from posts.views import PostListAPIView,PostsCreateAPIView,PostDetailAPIView, UpdateDetailAPIView
from comments.views import CommentListAPIView, CommentDetailAPIView, CommentCreateAPIView

# from django.conf.urls.static import static


urlpatterns = patterns('',
#API

    url(r'^api/register',
        AccountCreateAPIView.as_view(),
        name='account_create_api'),

    url(r'^api/posts/$',
        PostListAPIView.as_view(),
        name='post_list_api'),

    url(r'^api/posts/like/(?P<post_slug>[\w-]+)',
        'posts.views.like',
        name='post_like_api'),

    url(r'^api/posts/(?P<post_slug>[\w-]+)',
        PostDetailAPIView.as_view(),
        name='post_detail_api'),

    url(r'^api/updatepost/(?P<post_slug>[\w-]+)',
        UpdateDetailAPIView.as_view(),
        name='post_update_api'),

    url(r'^api/newpost',
        PostsCreateAPIView.as_view(),
        name='posts_create_api'),

    url(r'^api/comments/$',
        CommentListAPIView.as_view(),
        name='posts_comments_api'),

    url(r'^api/comments/(?P<id>\d+)/$',
        CommentDetailAPIView.as_view(),
        name = 'comment_detail_api'),

    url(r'^api/newcomment/$',
        CommentCreateAPIView.as_view(),
        name = 'comment_create_api'),




    url(r'^dj/admin/',
        include(admin.site.urls)),

    url(r'^api/auth/token/$','rest_framework_jwt.views.obtain_jwt_token'),

    url(r'^api/auth/',include('rest_framework.urls', namespace='rest_framework')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$',
        'patientfirst.views.home',
        name='home'),
    #Accounts
    url(r'^login/$',
        'patientfirst.views.auth_login',
        name='login'),
    url(r'^logout/$',
        'patientfirst.views.auth_logout',
        name='logout'),
    url(r'^register/$',
        'accounts.views.register',
        name='register'),
    #Posts
    url(r'^newpost/$',
        'posts.views.new_post',
        name='newpost'),
    url(r'^like/$', 'ranking.views.like', name='like'),
    url(r'^post/(?P<post_slug>[\w-]+)/$', 'posts.views.category_detail', name = 'post_detail'),
    url(r'^comment/$', 'comments.views.new_comment', name='comment'),
    url(r'^comment/(?P<id>\d+)/$', 'comments.views.comment_thread', name='comment_thread'),

    #Notifications
    url(r'^notifications/$', 'notification.views.all_notifications', name='notifications'),
    url(r'^notifications/mark_all_read$', 'notification.views.mark_all_read', name='notifications'),
    url(r'^notifications/mark_all_unread$', 'notification.views.mark_all_unread', name='notifications'),
    url(r'^notifications/read/(?P<notification_id>\d+)/$', 'notification.views.read', name='notifications_read'),
    url(r'^notifications/absurl/$', 'notification.views.mark_all_unread', name='notifications'),

                       )

if settings.DEBUG:
    urlpatterns += patterns('',) + static(settings.STATIC_URL, document_root= settings.STATIC_ROOT)
    urlpatterns += patterns('',) + static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)