import re

def slug_to_string(slug):
    slug = re.sub(r"([a-z])\-([a-z])", r"\1 \2", slug, 0, re.IGNORECASE | re.DOTALL)
    return slug


