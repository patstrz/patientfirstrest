
from rest_framework import routers, serializers, viewsets, permissions


from .models import MyUser



class AccountsSerializer(serializers.HyperlinkedModelSerializer):
    # url = VideoUrlField(view_name='video_detail_api')
    # category_title = serializers.CharField(source='category.title', read_only=True)
    # category_image = serializers.CharField(source='category.get_image_url', read_only=True)
    # # category = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all())
    # # category = CategorySerializer(many=False,read_only=True)
    # comment_set = CommentSerializer(many=True,read_only=True)
    password = serializers.CharField(max_length=100)


    def validate_password(self, value):
        if len(value) < 5:
            raise serializers.ValidationError("password must be 5 characters or longer")
        return value


    class Meta:
        model = MyUser
        fields =[
            'username',
            'email',
            'password',
        ]

