from django.shortcuts import render,Http404, redirect
from django.core.urlresolvers import reverse
from .forms import RegisterForm
from .models import MyUser
from .serializers import AccountsSerializer


from .forms import LoginForm, RegisterForm
from .models import MyUser

from rest_framework import generics, mixins, permissions


class AccountCreateAPIView(generics.CreateAPIView):
    serializer_class = AccountsSerializer
    permission_classes = [permissions.AllowAny, ]

# Rest API above ^

def register(request):

    form = RegisterForm(request.POST or None)

    next_url = request.GET.get('next')

    if form.is_valid():
        email = form.clean_email()
        username = form.clean_username()
        password = form.clean_password2()

        new_user = MyUser.objects.create_user(email=email, username=username, password=password)
        if new_user:
            return redirect(reverse("home"))

        else:
            return Http404

    context = {"form": form}
    return render(request, "accounts/register.html", context)