from django.shortcuts import render
from .models import Notification
from django.shortcuts import render, HttpResponseRedirect


def all_notifications(request):

    notifications = Notification.objects.all_for_user(request.user)
    targetless = Notification.objects.mark_targetless_unread(request.user)

    context = { 'notifications' : notifications}

    return render(request,'notifications/all.html', context)

def mark_all_read(request):

    Notification.objects.mark_all_read(request.user)

    return HttpResponseRedirect('/notifications')


def mark_all_unread(request):

    Notification.objects.mark_all_unread(request.user)

    return HttpResponseRedirect('/notifications')

def read(request, notification_id):
    next  = request.GET.get('next',None)
    print 'nexttype=',type(next)

    Notification.objects.mark_read(request.user, notification_id)

    if next is not None:

        return HttpResponseRedirect(next)

    else:
        return HttpResponseRedirect('/notifications')

    # return HttpResponseRedirect('/notifications')


#todo need to create absolte url for comments/post models, and use next? parameter
#todo to navigate to it

