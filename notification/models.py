from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.core.urlresolvers import reverse
from .signals import notify
from django.utils.text import slugify


class NotificationsQuerySet(models.query.QuerySet):
    def get_user(self, user):
        return self.filter(recipient=user)

    def get_notification(self,notification_id):
        notification = self.get(id=notification_id)
        return notification

    def read(self):
        return self.filter(read=True)

    def unread(self):
        return self.filter(read=False)

    def mark_all_read(self, recipient):
        qs = self.unread().get_user(recipient)
        qs.update(read=True)

    def mark_all_unread(self, recipient):
        qs = self.read().get_user(recipient)
        qs.update(read=False)

    def mark_targetless_unread(self, recipient):
        qs = self.unread().get_user(recipient)
        qs_targetless = qs.filter(target_object_id = None)
        if qs_targetless:
            qs_targetless.update(read=True)

    def mark_read(self,user,id):
        notification = self.get_notification(id)
        if user == notification.recipient:
            # notification.update(read=True)
            notification.read = True
            notification.save()
        else:
            raise ValueError('This notification does not belong to user')
#todo This^ valueError is for development only, potential security issue

#todo Later: need to make together unique clause and call error/message.




class NotificationManager(models.Manager):
    def get_queryset(self):
        return NotificationsQuerySet(self.model, using=self._db)

    def all_read(self,user):
        return self.get_queryset().get_user(user).read()

    def all_unread(self,user):
        return self.get_queryset().get_user(user).unread()

    def all_for_user(self,user):
        # self.get_queryset().mark_all_read(user)
        return self.get_queryset().get_user(user)

    def mark_all_read(self,user):
        return self.get_queryset().mark_all_read(user)

    def mark_all_unread(self,user):
        return self.get_queryset().mark_all_unread(user)

    def mark_targetless_unread(self, user):
        return self.get_queryset().mark_targetless_unread(user)

    def mark_read(self, user, id):
        return self.get_queryset().mark_read(user, id)





class Notification(models.Model):

     # sender = models.ForeignKey()
    # instance of a user or object
    sender_content_type = models.ForeignKey(ContentType, related_name='notify_sender')  # could be notify actor
    sender_object_id = models.PositiveIntegerField()
    sender_object = GenericForeignKey("sender_content_type", "sender_object_id")

    # action that is being carried out, i.e) new comment, new video ...
    verb = models.CharField(max_length=255)

    # actual action, i.e) response to comment, adding a new comment on a video ...
    action_content_type = models.ForeignKey(ContentType, related_name='notify_action',
        null=True, blank=True)
    action_object_id = models.PositiveIntegerField(null= True, blank =True)
    action_object = GenericForeignKey("action_content_type", "action_object_id")

    # Who/ what you are going to tell that an action happend, could be a video or user ...
    target_content_type = models.ForeignKey(ContentType, related_name='notify_target',null= True, blank =True)
    target_object_id = models.PositiveIntegerField(null= True, blank =True)
    target_object = GenericForeignKey("target_content_type","target_object_id")


    recipient = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='notification')
    read = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    objects = NotificationManager()

    def __unicode__(self):

        context = {
            "sender": self.sender_object,
            "verb": self.verb,
            "action": self.action_object,
            "target": self.target_object,
        }

        if self.action_object:
            if self.target_object:
                return "%(sender)s %(verb)s on %(target)s with %(action)s" % context
            return "%(sender)s %(verb)s with %(action)s" % context
        return "%(sender)s %(verb)s " % context

    def get_absolute_url(self):
        if self.target_object_id is not None:
            return reverse('notifications_read', kwargs={"notification_id": self.id})
        else:
            return '/notifications'
         #todo: make this else url more robust i.e) fetch current path


def new_notification(sender, *args, **kwargs):
    recipient = kwargs.pop('recipient')
    verb = kwargs.pop('verb')
    new_note = Notification(
        recipient=recipient,
        verb=verb,
        sender_content_type=(ContentType.objects.get_for_model(sender)),
        sender_object_id = sender.id,
    )

    for option in ("target","action"):
        obj = kwargs.pop(option, None)
        if obj is not None:
            setattr(new_note, "%s_content_type" %option, ContentType.objects.get_for_model(obj))
            setattr(new_note,"%s_object_id" %option, obj.id)
    new_note.save()

notify.connect(new_notification)
