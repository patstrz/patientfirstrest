from django.http import HttpResponse
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from posts.models import Post

@login_required
@require_POST
def like(request):
    if request.method == 'POST':
        user = request.user
        slug = request.POST.get('slug', None)
        item = get_object_or_404(Post, slug=slug)

        if item.likes.filter(id=user.id).exists():
            # user has already liked this company
            # remove like/user
            item.likes.remove(user)
            message = 'disliked'

        else:
            # add a new like for a company
            item.likes.add(user)
            message = 'liked'
        print slug
        print user, slug, item.total_likes()
        likes_count = item.total_likes()

    context = {'likes_count': item.total_likes(), 'message':message, }

    return HttpResponse(json.dumps(context), content_type='application/json')

