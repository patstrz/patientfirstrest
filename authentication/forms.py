from django import forms

class SignUp(forms.Form):

    username = forms.CharField(label="Username")
    password = forms.CharField(widget = forms.PasswordInput )
