# README #

‘patientfirst' is the main project directory with settings.py, and urls

Messy repo with mixed code, **API code** is in:
```
#!python

 posts/serializers, posts/views, comments/serializers, comments/views
```


checkout the **hosted version** http://patientfirstrest3.herokuapp.com/api/posts/ ( still have it in development mode) 

**login credentials**:


```
#!python

username: test
password: 123
```


**API endpoints:**


```
#!python

/api/posts
```


to update a post must go to the 
```
#!python

/api/updateposts/post_slug 
```
endpoint ( where you provide the post_slug, which is the slug value from an individual post) 
sample url: /api/updatepost/cerebral-palsy-walking-19


```
#!python

/api/comments
```

has urls to individual comments ( hyperlink to the ‘post_detail_api’ view) --> Can **update/delete** here